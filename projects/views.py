from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm
from django.urls import reverse


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    tasks = Task.objects.filter(project=project)
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect(reverse("list_projects"))
    else:
        form = ProjectForm()
    return render(request, "projects/create.html", {"form": form})
